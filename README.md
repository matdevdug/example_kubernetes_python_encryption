### Example Kubernetes Secret Generation

1. Make a .env file with the following parameters 
KEY=Make a fernet key: https://fernetkeygen.com/
CLUSTER_NAME=name_of_cluster_you_want_to_use
SECRET-TEST-1=9e68b558-9f6a-4f06-8233-f0af0a1e5b42
SECRET-TEST-2=a004ce4c-f22d-46a1-ad39-f9c2a0a31619
SECRET-TEST-3=933e30f5-8d9c-4041-b5eb-d68ed3ff47c0

2. Tested with Python3.12, I've included a basic Dockerfile that also installs Rust for the cryptography library. You'll need to handle copying the kubeconfig file for the cluster you want to use. 
3. It'll generate a CSV file with the names of the secret you added to the .env file and the encrypted output, then it'll take them and move them into the cluster as secrets. 
4. You'll need to configure your application to decrypt the secret when it mounts it into the cluster. 

