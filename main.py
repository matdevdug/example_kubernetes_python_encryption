from kubernetes import client, config
from kubernetes.client import configuration
from cryptography.fernet import Fernet
from dotenv import load_dotenv
import os
import base64
import csv
import re

load_dotenv()
fernet = Fernet(os.environ['KEY'])
cluster_name = os.environ['CLUSTER_NAME']
contexts, active_context = config.list_kube_config_contexts()

def is_valid_secret_name(string):
    pattern = r"[a-z0-9]([-a-z0-9]*[a-z0-9])?(\\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*"
    return bool(re.match(pattern, string))

def get_dev_context(contexts, cluster_name):
    for context in contexts:
        if cluster_name in context['name']:
            return context['name']
        else:
            continue

def read_secrets_from_csv(filename="secret_env_vars.csv"):
    secrets = {}
    if os.path.exists(filename):
        with open(filename, "r", newline="") as csvfile:
            reader = csv.reader(csvfile)
            next(reader)  # Skip header row
            for row in reader:
                secrets[row[0]] = row[1]

    return secrets

def find_secret_env_vars(fernet, filename="secret_env_vars.csv"):
    existing_data = []

    if os.path.exists(filename):
        try:
            with open(filename, "r", newline="") as csvfile:
                reader = csv.reader(csvfile)
                next(reader)  # Skip header row
                for row in reader:
                    existing_data.append(row[0])  # Store existing key names
        except FileNotFoundError:
            pass

    secret_vars = {}
    for key, value in os.environ.items():
        if "SECRET" in key.upper():
            # Check for duplicate key name in existing data
            if key in existing_data:
                print(f"Duplicate key found: {key}")
                continue
            secret_vars[key] = value

    with open(filename, "a", newline="") as csvfile:
        writer = csv.writer(csvfile)
        if not existing_data:  # Write header only if no existing data
            writer.writerow(["Variable", "Value"])

        for key, value in secret_vars.items():
            encrypted_value = fernet.encrypt(value.encode())

            writer.writerow([key, encrypted_value.decode()])

    print(f"Secret environment variables appended to: {filename}")

def check_secret_exists(secret_name, namespace_to_use="default"):
    v1_api = client.CoreV1Api()
    secrets = v1_api.list_namespaced_secret(namespace=namespace_to_use)

    for secret in secrets.items:
        if secret.metadata.name == secret_name:
            return True

    return False

def create_secrets_from_csv(filename="secret_env_vars.csv", namespace_to_use="default"):
    secrets = read_secrets_from_csv(filename)
    v1_api = client.CoreV1Api()

    for key, value in secrets.items():
        sec = client.V1Secret()
        sec.metadata = client.V1ObjectMeta(name=key.lower(), namespace=namespace_to_use)
        sec.type = "Opaque"
        sec.immutable = True
        sec.data = {key: base64.b64encode(value.encode()).decode()}

        if not is_valid_secret_name(key.lower()):
            print(f"Invalid secret name: {key}")
            continue # Skip creating secret if name is invalid

        if check_secret_exists(key.lower(), namespace_to_use):
            print(f"Secret {key} already exists!")
            continue
        else:
            v1_api.create_namespaced_secret(body=sec, namespace=namespace_to_use)
            print(f"Secret {key} created successfully!")

find_secret_env_vars(fernet, filename="secret_env_vars.csv")
config.load_kube_config(context=get_dev_context(contexts, cluster_name))
create_secrets_from_csv(filename="secret_env_vars.csv", namespace_to_use="default")
