FROM python:3.12-slim

# Install basic build tools for Rust
RUN apt-get update && \
    apt-get install -y curl build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget llvm \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install Rust using rustup
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

# Set PATH for Rust tools
ENV PATH="/root/.cargo/bin:$PATH"

# Install Python dependencies from requirements.txt
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt

ENTRYPOINT ["python"]  # Set the default entrypoint to run Python scripts
